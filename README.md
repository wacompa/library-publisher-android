# Wacom Library Publisher Script
Script which holds the publishing tasks for the **Artifactory** and **BitBucket** Maven repositories.

### Configure Project for Artifact Uploading
 - In your `local.properties` file add the following fields:
``` 
# Artifactory Credentials
artifactoryUsername={YOUR_ARTIFACTORY_USERNAME}
artifactoryPassword={YOUR_ARTIFACTORY_PASSWORD}

# BitBucket Credentials
bitBucketUsername={YOUR_BITBUCKET_USERNAME}
bitBucketPassword={YOUR_BITBUCKET_PASSWORD}
```
  
 - Add the following to your `project` level `build.gradle`:
 
```
buildscript {
    dependencies {
         classpath "org.jfrog.buildinfo:build-info-extractor-gradle:4.8.1"
    }
}

```
 - Create a `publishing.properties` file in your `app` module and add the following properties:
 
```
# Artifact Metadata
ARTIFACT_VERSION={ARTIFACT_VERSION}
ARTIFACT_NAME={ARTIFACT_NAME}
ARTIFACT_PACKAGE={ARTIFACT_PACKAGE}
ARTIFACT_PACKAGING=aar
# Repository Path
COMPANY=wacompa
REPOSITORY_NAME=maven_repository
``` 

 - Add the following to your `app` level `build.gradle`:
```
// Publishing
def BASE_URL = "https://api.bitbucket.org/2.0/repositories/wacompa/library-publisher-android/src/master"
def PROJECT_PATH = "/publisher/src"
apply from: "$BASE_URL$PROJECT_PATH/publisher.gradle"

ext {
    final Properties properties = new Properties()
    properties.load(project.file('publishing.properties').newDataInputStream())

    ARTIFACT_VERSION = properties.getProperty('ARTIFACT_VERSION')
    ARTIFACT_NAME = properties.getProperty('ARTIFACT_NAME')
}
```

 - Lastly go to your project's Gradle tasks and you should be able to see a group called`deploy`,
 inside which there should be a task called `deployArtifacts`.